package functions_test

import (
	"fmt"
	"gitee.com/cubelab/tcb-openapi"
	"gitee.com/cubelab/tcb-openapi/config"
	"github.com/spf13/viper"
	"testing"
	"time"
)

var client *tcb.Tcb

//初始化
func init() {
	envId := viper.GetString("env_id")
	//小程序handle
	client = tcb.NewTcb(&config.Config{
		EnvId:     envId,
		Timeout:   time.Duration(15) * time.Second,
		LogPrefix: viper.GetString("sts_name"),
		Debug:     viper.GetBool("tcb_open_api_debug"),
	})
}

func TestFunction(t *testing.T) {
	fmt.Println(client.GetFunction().Invoke("test", map[string]interface{}{
		"data": map[string]interface{}{
			"path":       "/ping",
			"httpMethod": "GET",
			"body":       "",
		},
	}))
}
