package tcb

import (
	"gitee.com/cubelab/tcb-openapi/component"
	"gitee.com/cubelab/tcb-openapi/component/database"
	"gitee.com/cubelab/tcb-openapi/component/functions"
	"gitee.com/cubelab/tcb-openapi/component/storage"
	"gitee.com/cubelab/tcb-openapi/config"
	"gitee.com/cubelab/tcb-openapi/context"
	"gitee.com/cubelab/tcb-openapi/util"
	"github.com/sirupsen/logrus"
	"os"
)

/*
Tcb 实例
*/
type Tcb struct {
	context *context.Context
	core    *component.Core
}

/*
创建实例
*/
func NewTcb(config *config.Config) *Tcb {
	//上下文
	ctx := &context.Context{
		Config: config,
		Logger: &logrus.Logger{
			Out:          os.Stdout,
			Formatter:    &util.CustomerFormatter{Prefix: config.LogPrefix},
			Level:        logrus.DebugLevel,
			ExitFunc:     os.Exit,
			ReportCaller: true,
		},
	}
	//cam
	return &Tcb{ctx, component.NewCore(ctx)}
}

//接入数据库
func (t *Tcb) GetDatabase() *database.Database {
	return database.NewDatabase(t.context, t.core)
}

//接入云函数
func (t *Tcb) GetFunction() *functions.Function {
	return functions.NewFunction(t.context, t.core)
}

//接入云存储
func (t *Tcb) GetStorage() *storage.Storage {
	return storage.NewStorage(t.context, t.core)
}
