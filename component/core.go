package component

import (
	"gitee.com/cubelab/tcb-openapi/context"
	"gitee.com/cubelab/tcb-openapi/http"
	"gitee.com/cubelab/tcb-openapi/util"
	"net/url"
)

type Core struct {
	ctx    *context.Context
	client *http.Client
}

func NewCore(ctx *context.Context) *Core {
	return &Core{ctx, http.NewHttpClient(ctx)}
}

//GET
func (c *Core) HttpGetJson(path string, params url.Values, headers ...map[string]string) (body string, err error) {
	//附加数据
	body, err = c.client.HttpGetJson(path, params, headers...)
	if err != nil {
		return
	}
	return
}

//POST
func (c *Core) HttpPostJson(path string, request interface{}, headers ...map[string]string) (body string, err error) {
	//附加数据
	body, err = c.client.HttpPostJson(path, util.JsonEncode(request), headers...)
	if err != nil {
		return
	}
	return
}

//PATCH
func (c *Core) HttpPatchJson(path string, request interface{}, headers ...map[string]string) (body string, err error) {
	//附加数据
	body, err = c.client.HttpPatchJson(path, util.JsonEncode(request), headers...)
	if err != nil {
		return
	}
	return
}

//DELETE
func (c *Core) HttpDeleteJson(path string, request interface{}, headers ...map[string]string) (body string, err error) {
	//附加数据
	body, err = c.client.HttpDeleteJson(path, util.JsonEncode(request), headers...)
	if err != nil {
		return
	}
	return
}

//PUT
func (c *Core) HttpPutJson(path string, request interface{}, headers ...map[string]string) (body string, err error) {
	//附加数据
	body, err = c.client.HttpPUTJson(path, util.JsonEncode(request), headers...)
	if err != nil {
		return
	}
	return
}
